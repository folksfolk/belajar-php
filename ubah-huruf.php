<?php
    function ubah_huruf($string){
        //kode di sini
        $abjad = "abcdefghijklmnopqrstuvwxyz";
        $tampung = "";
        for($i = 0; $i < strlen($string); $i++) {
            $posisi = strpos($abjad, $string[$i]);
            $tampung .= substr($abjad, $posisi + 1, 1);
        }
        echo "<br>";
        return $tampung;
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

?>